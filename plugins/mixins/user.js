import Vue from 'vue'
import { mapGetters } from 'vuex'

const User = {
    install (Vue, Options) {
        Vue.mixin({
            computed: {
                ...mapGetters({
                    user: 'user',
                    follow : 'follow',
                    basket : 'basket',
                    path : "path"
                })
            }
        })
    }
}

Vue.use(User);
