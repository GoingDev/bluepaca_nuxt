import Vue from 'vue'
import { mapGetters } from 'vuex'

const lang = {
    install (Vue, Options) {
        Vue.mixin({
            computed: {
                ...mapGetters({
                    lang: 'lang/locale'
                })
            }
        })
    }
}

Vue.use(lang);
