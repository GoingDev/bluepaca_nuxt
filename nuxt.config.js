import pkg from './package'
import axios from 'axios'

export default {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: 'Bluepaca',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'เว็ปไซต์ขายเครื่องสำอางค์ Bluepaca'
      }
    ],
    link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/blogo.ico'
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Kanit"
      },
      {
        rel: "stylesheet",
        href: "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      },
      {
        rel: "stylesheet",
        href: "https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
      }
    ],
  },

  webfontloader: {
    google: {
      families: ['Kanit:400,700'] //Loads Lato font with weights 400 and 700
    }
  },

  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/loading.vue',

  /*
   ** Global CSS
   */
  css: [
    "@/assets/style/style.scss"
  ],

  router: {
    middleware: ['i18n']
  },

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/moment.js'},
    { src: '~/plugins/client/alert', ssr: false},
    { src: '~/plugins/client/awesome', ssr: false},
    { src: '~/plugins/client/slick', ssr: false},
    { src: '~/plugins/client/paginate', ssr: false},
    { src: '~/plugins/client/thailand', ssr: false},
    '~/plugins/i18n.js',
    '~/plugins/lodash.js',
    '~/plugins/mixins/lang.js',
    '~/plugins/mixins/global.js',
    '~/plugins/mixins/user.js',
    // '~/plugins/https.js',
    '~/plugins/veevalidate.js',
    '~/plugins/axios'
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    'nuxt-webfontloader',
    'cookie-universal-nuxt'
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: 'http://localhost:3300/api',
    retry: {
      retries: 2
    }
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  env: {
    pathServer: 'http://localhost:3300',
    appLocale: process.env.APP_LOCALE || 'th',
  }
}

//https://phpstack-155228-743982.cloudwaysapps.com
