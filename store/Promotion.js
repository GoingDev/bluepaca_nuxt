const state = () => ({
  flashsale: null
})

const getters = {
  flashsale : (state) => (name) =>{
    return state.flashsale;
  }
}

const mutations = {
  setFlashsale(state, data) {
    state.flashsale = data;
  }
}

const actions = {
  async getFlashsale({
    commit
  }) {
    await this.$axios.$get('flashsalenow').then((res) => {
        commit('setFlashsale', res)
      })
      .catch((err) => {
        console.log(err)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
