const state = () => ({
  content: []
})

const getters = {
  contentByName : (state) => (name) =>{
    return state.content.find((q)=>q.content_name === name);
  }
}

const mutations = {
  setContent(state, data) {
    state.content = data;
  }
}

const actions = {
  async getContent({
    commit
  }) {
    await this.$axios.$get('content').then((res) => {
        commit('setContent', res)
      })
      .catch((err) => {
        console.log(err)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
