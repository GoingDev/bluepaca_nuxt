const state = () => ({
  news : []
})

const getters = {
  newsPublic(state){
    return state.news.filter((q)=>q.news_status == 1);
  },
  newsByID : (state) => (id) => {
    return state.news.find((q)=>q.id == id);
  }
}

const mutations = {
  setNews(state,data){
    state.news = data
  }
}

const actions = {
  async getNews({commit}){
    await this.$axios.$get('news').then((res)=>{
      commit('setNews',res);
    })
    .catch((err)=>{

    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
